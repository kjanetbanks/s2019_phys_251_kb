#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
===============================================================================
Linear fit using scipy.curve_fi()
https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html#scipy.optimize.curve_fit
-------------------------------------------------------------------------------
Created on January 2018
@author: Fernando Camelli
===============================================================================
"""
#===========================================================================================================
# we need to define the straight line function first
def myline(x,m,b):
    y=m*x+b
    return y

#===========================================================================================================
#===========================================================================================================
#===========================================================================================================
# script
#===========================================================================================================
import numpy as np                    # we import numpy
import matplotlib.pyplot as plt       # loading pyplot from matplotlib
from scipy.optimize import curve_fit  # fitting

dname=r'class_data_01'  # directory where the file is located
        # please observe the r in front of 
        # the first quotation
        # https://docs.python.org/3/reference/lexical_analysis.html#literals
        # 'r' i a string literal: if 'r' is not present the escape characters are interpreted according to
        # the Standard C rules
fname='velocity_run018.txt'                       # name of the file - include the extension
dfname ='{0:s}/{1:s}'.format(dname,fname)         # concatenate directory and file name

# further information on genfromtxt:
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.genfromtxt.html
velo18=np.genfromtxt(dfname,skip_header=1)   # velo18 is a ndarray
                                             # skip one header -> skip_header=1
    
t =velo18[:,0]   # gather time
v =velo18[:,1]   # gather velocity
dv=velo18[:,2]   # gather uncertainty of velocity

# fit without considering the uncertainty
popt0,pcov0 = curve_fit(myline,t,v)
print('Without using uncertainty of the velocity')
print('Optimization parameter:')
print(popt0)
print('Covariance matrix:')
print(pcov0)
print('\n')

# fit without considering the uncertainty
popt1,pcov1 = curve_fit(myline,t,v,sigma=dv)
print('Using uncertainty of the velocity')
print('Optimization parameter:')
print(popt1)
print('Covariance matrix:')
print(pcov1)
print('\n')


# plot
m0=popt0[0]   # gather m and b - calculattion without uncertainty
b0=popt0[1]

m1=popt1[0]   # gather m and b - calculattion with uncertainty
b1=popt1[1]

yhat0=m0*t+b0 # get values of line - calculation without uncertainty
yhat1=m1*t+b1 # get values of line - calculation with    uncertainty

plt.rc('text', usetex=True)      # using Latex rendering 

# https://matplotlib.org/api/_as_gen/matplotlib.pyplot.errorbar.html
# plot data and error bars
plt.errorbar(t,v,yerr=dv,fmt='dk',capsize=5,ecolor='r',label='Experimental data')  
plt.grid(True)                 # add grid
plt.xlabel(r'$time\,\,(s)$')   # label in x: time
plt.ylabel(r'$v\,\,(m/s)$')    # label in y: velocity
plt.plot(t,yhat0,label=r'without $\sigma$')
plt.plot(t,yhat1,label=r'with $\sigma$')
plt.legend()                   # show legend                                  
plt.show()                     # show plot
