#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 11:19:26 2019

@author: kimbanks
"""
n=1000

x=0
for i in range(n):
    if i % 3 ==0 or i % 5 ==0: #evenly divisible by 3 OR evenly divisible by 5
        x=x+i
    else:
        pass
print(x)