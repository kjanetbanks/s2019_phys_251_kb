#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 11:39:59 2019

@author: kimbanks
"""
# =============================================================================
# Try #2 HW5_1.py
# =============================================================================
import matplotlib.pyplot as plt 
from scipy.optimize import curve_fit
import pandas as pd  # import pandas
import numpy as np
dname=r'/Users/kimbanks/s2019_phys_251_kb'                        # directory where the file is located
fname='velocity_run18.txt'                   # name of the file - include the extension
dfname ='{0:s}/{1:s}'.format(dname,fname)     # concatenate directory and file name

data01 = np.genfromtxt(dfname,skip_header=1)

t=data01[:,0]
v=data01[:,1]
u=data01[:,2]
print(data01)
print('This {}= Time'.format(t))
print('This {}= Velocity'.format(v))

plt.plot(t,v, label= 'Time vs Velocity')
plt.xlabel('Time(s)')
plt.ylabel('Velocity(m/s)')
plt.errorbar(t, v, yerr=u, fmt='.k')
plt.title('Time vs Velocity with Uncertainty')
plt.grid()
plt.legend(loc='upper left')
plt.savefig('Figure1.png')
plt.show



# create arrays
t=t
v=v
n=t.size
# using numpy.interp
ti=n
vi=np.interp(ti,t,v)

print('t_i = {} - v_i = {}'.format(ti,vi))

y=0
x=0
for i in range(n):
    y=(vi+v)/2
    x=((vi+v)/2)
    
plt.plot(x,y, label= 'X vs Y')
plt.xlabel('X')
plt.ylabel('y')
plt.title('Position')
plt.grid()
plt.legend(loc='upper left')
plt.savefig('Figure2.png')
plt.show    
    


p,pcov= curve_fit(t,v,u)
plt.plot(t,v, label= 'Time vs Velocity')
plt.xlabel('Time(s)')
plt.ylabel('Velocity(m/s)')
plt.errorbar(t, v, yerr=u, fmt='.k')
plt.title('Time vs Velocity with Uncertainty')
plt.grid()
plt.legend(loc='upper left')
plt.savefig('Figure3.png')
plt.show


#x=pd.DataFrame(data=data01,columns=['X'])
#y=pd.DataFrame(data=data01,columns=['Y'])
#data01.plot(kind='scatter',x='time(s)',y='velocity(m/s)',grid=True)



