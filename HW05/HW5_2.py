#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  4 12:07:05 2019

@author: kimbanks
"""

import matplotlib.pyplot as plt 
from scipy.optimize import curve_fit
import pandas as pd  # import pandas
import numpy as np
dname=r'/Users/kimbanks/s2019_phys_251_kb'                        # directory where the file is located
fname='fc_thist_00520.txt'                   # name of the file - include the extension
dfname ='{0:s}/{1:s}'.format(dname,fname)     # concatenate directory and file name

data01 = np.genfromtxt(dfname,skip_header=2)

t=data01[:,0]
P=data01[:,1]
Vx=data01[:,2]
Vy=data01[:,3]
Vz=data01[:,4]
N=data01[:,5]
T=data01[:,6]
s1=data01[:,7]
s2=data01[:,8]
s3=data01[:,9]
s4=data01[:,10]
s5=data01[:,11]

print(data01)


plt.plot(s1,t, label= 'S1 vs Time')
plt.xlabel('S1')
plt.ylabel('Time')

plt.title('S1 vs Time')
plt.grid()
plt.legend(loc='upper left')
plt.savefig('Figure3.png')
plt.show



## create arrays
#t=t
#v=v
#n=t.size
## using numpy.interp
#ti=n
#vi=np.interp(ti,t,v)
#
#print('t_i = {} - v_i = {}'.format(ti,vi))
#
#y=0
#x=0
#for i in range(n):
#    y=(vi+v)/2
#    x=((vi+v)/2)
#    
#plt.plot(x,y, label= 'X vs Y')
#plt.xlabel('X')
#plt.ylabel('y')
#plt.title('Position')
#plt.grid()
#plt.legend(loc='upper left')
#plt.savefig('Figure2.png')
#plt.show    
#    


p,pcov= curve_fit(t,v,u)
plt.plot(t,v, label= 'Time vs Velocity')
plt.xlabel('Time(s)')
plt.ylabel('Velocity(m/s)')
plt.errorbar(t, v, yerr=u, fmt='.k')
plt.title('Time vs Velocity with Uncertainty')
plt.grid()
plt.legend(loc='upper left')
plt.savefig('Figure3.png')
plt.show