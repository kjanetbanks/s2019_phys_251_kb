#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  8 14:32:11 2019
HW2_3.py
KIM BANKS
@author: kimbanks
"""
import numpy as np

a=np.arange(1,101) #creating array of 1-100

B=a.reshape(10,10) #making array into a matrix of 10 rows and 10 columns

print ('B={}'.format(B))

#now create C

BU=np.triu(B) #taking upper part of triangle of B
BL=np.tril(B) #taking lower part of triangle of B

#print(BU) looks good
#print(BL) looks good

C= np.zeros(100).reshape(10,10) #empty C





print('C={}'.format(C))