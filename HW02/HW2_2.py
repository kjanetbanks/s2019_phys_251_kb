#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  8 11:04:28 2019
HW2_2.py
KIM BANKS
@author: kimbanks
"""

#FOR LOOPS AND ARRAYS

for i in range(1,101):

   if i>=40 and i<=60:
       if (-1)**i != 1:
           print('Odd Numbers 40-60: {}'.format(i))

#what i got after running code
#Odd Numbers 40-60: 41
#Odd Numbers 40-60: 43
#Odd Numbers 40-60: 45
#Odd Numbers 40-60: 47
#Odd Numbers 40-60: 49
#Odd Numbers 40-60: 51
#Odd Numbers 40-60: 53
#Odd Numbers 40-60: 55
#Odd Numbers 40-60: 57
#Odd Numbers 40-60: 59


