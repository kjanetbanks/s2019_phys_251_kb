#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 14:32:55 2019
HW3_2_2.py
Kim Banks
@author: kimbanks
"""


import matplotlib.pyplot as plt
import math as m
import numpy as np

#x from -4 to 4 w/ min 100 data points

x=np.linspace(-4,4,100)

n=x.size #defining size of x

f2a=np.empty(n) 
f2b=np.empty(n)#making same sizes as x
#creating equation

for i in range(x.size): #same size
 f2a[i]=m.sin(2*x[i])
 f2b[i]=x[i]**3/10+x[i]**2/10

#if f2a[i] == f2b[i]: #finding intersection
    #print('Intersects at {}'.format(x)) 
#didn't work    
 #plot graph
plt.plot(x,f2a,'-')
plt.plot(x,f2b,'.')
plt.ylabel(r'$f2a AND f2b$')
plt.xlabel(r'$x$')
plt.grid(which='major',color='gray')
plt.title(r'$f2a AND f2b versus x$')
plt.savefig('HW3_2_2.png',dpi=300)
plt.legend('ab',loc='upper right')
plt.show()
#ESTIMATE INTERSEPTION: x=-2.5 and x= 1.3

