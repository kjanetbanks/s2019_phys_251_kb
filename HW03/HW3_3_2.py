#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 21 00:06:35 2019
HW3_3_2.py
Kim Banks
@author: kimbanks
"""


import matplotlib.pyplot as plt
import math as m
import numpy as np

#listing knowns:
g= 9.8 #m/s^2 gravity
v0=40.0  #m/s intial velocity
A=45     #ALPHA intial angle in degrees
v0x=v0*m.cos(A) #v in x direction
v0y=v0*m.sin(A) #v in y direction
x0=0   #starting point in x
y0=0   #starting point in y

#min 100 data points

t=np.linspace(0,7,100) #time in seconds

n=t.size #defining size of t

vx=np.empty(n) 
vy=np.empty(n) #making same sizes as t

#creating equations

for i in range(t.size): #same size as t
    vx[i]=v0x
    vy[i]=v0y-g*t[i]


#plotting vx vs t and vy vs t
plt.plot(t,vx,'-')
plt.plot(t,vy,'.')
plt.ylabel(r'$vx AND vy (METERS/SEC)$')
plt.xlabel(r'$TIME IN (SECONDS)$')
plt.grid(which='major',color='red')
plt.title(r'$vx AND vy VS time$')
plt.savefig('HW3_3_2.png',dpi=300)
plt.legend('xy',loc='upper left')
plt.show()

#ESTIMATION: vy=0 at t= 3.5s