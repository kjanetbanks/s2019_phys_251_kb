#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 14:33:36 2019
HW3_2_3.py
Kim Banks
@author: kimbanks
"""

import matplotlib.pyplot as plt
import math as m
import numpy as np

#x from -4 to 4 w/ min 100 data points

x=np.linspace(-4,4,100)

n=x.size #defining size of x

f3a=np.empty(n) 
f3b=np.empty(n)#making same sizes as x
#creating equation

for i in range(x.size): #same size
 f3a[i]=m.e**(0.001*x[i])
 f3b[i]=0.1*x[i]**3+0.1*x[i]**2-5
 
#if f3a[i] == f3b[i]: #finding intersection
    #print('Intersects at {}'.format(x)) 
#didn't work
plt.plot(x,f3a,'-')
plt.plot(x,f3b,'.')
plt.ylabel(r'$f3a AND f3b$')
plt.xlabel(r'$x$')
plt.grid(which='major',color='green')
plt.title(r'$f3a AND f3b versus x$')
plt.savefig('HW3_2_3.png',dpi=300)
plt.legend('ab',loc='upper right')
plt.show()

#ESTIMATE INTERSEPCTION: X=3.5
