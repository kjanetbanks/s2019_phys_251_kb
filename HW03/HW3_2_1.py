#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 14:16:55 2019
HW3_2_1.py
Kim Banks
@author: kimbanks
"""


import matplotlib.pyplot as plt
import math as m
import numpy as np

#x from -4 to 4 w/ min 100 data points

x=np.linspace(-4,4,100)

n=x.size #defining size of x

f1a=np.empty(n) 
f1b=np.empty(n)#making same sizes as x
#creating equation

for i in range(x.size): #same size
 f1a[i]=m.sin(x[i])
 f1b[i]=x[i]**2

plt.plot(x,f1a,'-')
plt.plot(x,f1b,'.')
plt.ylabel(r'$f1a AND f1b$')
plt.xlabel(r'$x$')
plt.grid(which='major',color='green')
plt.title(r'$f1a AND f1b versus x$')
plt.savefig('HW3_2_1.png',dpi=300)
plt.legend('ab',loc='upper right')
plt.show()

















