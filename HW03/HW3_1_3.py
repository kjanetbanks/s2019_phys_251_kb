#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 11:32:18 2019
Hw3_1.py
Kim Banks
@author: kimbanks
"""

import matplotlib.pyplot as plt
import math as m
import numpy as np

#theta from -pi tp pi w/ 50 element

theta=np.linspace(-m.pi,m.pi,50)

n=theta.size #defining size of theta
f3=np.empty(n) #making same size as theta
#creating equation

for i in range(theta.size): #same size
 f3[i]=m.cos(2*theta[i])


plt.plot(theta,f3,'-')
plt.ylabel(r'$f3$')
plt.xlabel(r'$theta$')
plt.grid(which='major',color='green')
plt.title(r'$f3=sec(theta)+tan(theta)$')
plt.savefig('HW3_1_3.png',dpi=300)
plt.legend('f3=_',loc='upper right')
plt.show()