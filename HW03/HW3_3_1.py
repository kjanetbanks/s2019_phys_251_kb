#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 20 23:28:28 2019
HW3_3_1.py
KimBanks
@author: kimbanks
"""


import matplotlib.pyplot as plt
import math as m
import numpy as np

#listing knowns:
g= 9.8 #m/s^2 gravity
v0=40.0  #m/s intial velocity
A=45     #ALPHA intial angle in degrees
v0x=v0*m.cos(A) #v in x direction
v0y=v0*m.sin(A) #v in y direction
x0=0   #starting point in x
y0=0   #starting point in y

#min 100 data points

t=np.linspace(0,7,100) #time in seconds

n=t.size #defining size of t

x=np.empty(n) 
y=np.empty(n) #making same sizes as t

#creating equations

for i in range(t.size): #same size as t
    x[i]=x0+v0x*t[i]
    y[i]=y0+v0y*t[i]-1/2*g*t[i]**2
 
plt.rc('text',usetex=True)

#plotting x vs t and y vs t
plt.plot(t,x,'-')
plt.plot(t,y,'.')
plt.ylabel(r'$x AND y (METERS)$')
plt.xlabel(r'$TIME IN (SECONDS)$')
plt.grid(which='major',color='blue')
plt.title(r'$x AND y VS time$')
plt.savefig('HW3_3_1.png',dpi=300)
plt.legend('xy',loc='upper left')
plt.show()


#ESTIMATION: ball hits ground as t=7s, x=150m, y=0