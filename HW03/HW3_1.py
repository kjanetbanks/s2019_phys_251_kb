#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 18 16:43:58 2019
HW3_1.py
Kim Banks
@author: kimbanks
"""

import matplotlib.pyplot as plt
import math as m
import numpy as np

#theta from -pi tp pi w/ 50 element

theta=np.linspace(-m.pi,m.pi,50)

n=theta.size #defining size of theta
f1=np.empty(n) #making same size as theta
#creating equation

for i in range(theta.size): #same size
 f1[i]=m.cos(2*theta[i])


plt.plot(theta,f1,'.')
plt.ylabel(r'$f1$')
plt.xlabel(r'$theta$')
plt.grid(which='major',color='red')
plt.title(r'$f1=cos(2theta)$')
plt.savefig('HW3_1_1.png',dpi=300)
plt.show()














