#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 10:35:38 2019
HW3_1.py
KimBanks
@author: kimbanks
"""

import matplotlib.pyplot as plt
import math as m
import numpy as np

#theta from -pi tp pi w/ 50 element

theta=np.linspace(-m.pi,m.pi,50)

n=theta.size #defining size of theta
f2=np.empty(n) #making same size as theta
#creating equation

for i in range(theta.size): #same size
 f2[i]=2*m.cos(theta[i])**2-1


plt.plot(theta,f2,'_')
plt.ylabel(r'$f2$')
plt.xlabel(r'$theta$')
plt.grid(which='major',color='black')
plt.title(r'$f2=2cos(theta)^2-1$')
plt.savefig('HW3_1_2.png',dpi=300)
plt.legend('f2=_',loc='upper right')
plt.show()