#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 29 11:13:20 2019
Kim Banks
HW1_4.py
@author: kimbanks
"""
#Write a Python function named sphereinfo(r). This function should take the radius r of a sphere
#as input argument and return the volume of the sphere in m3 and the surface area of the sphere
#in m2
# You should call the function from a script to test it with r = 10.0 meters and r = 1.0 meter.

import math as m #to use for pi

def sphereinfo(r): 
    
    V =(4/3)*m.pi*r**3 #create V formula
   
    #have them both with return at same time
    #previous error: having two separate returns
    
    SA =(4/3)*m.pi*r**2 #create SA formula
    
    
    return V, SA


r = 1.0 # r=10.0 prints out as well
   
V,SA= sphereinfo(r)  #calling function

print('Volume in meters cubed {} and Surface Area in meters squared {}'.format(V,SA))
    
 #prints as I wanted it to--->"runfile('/Users/kimbanks/Desktop/s2019_phys_251_kb/HW1_4.py', wdir='/Users/kimbanks/Desktop/s2019_phys_251_kb')
#   Volume in meters cubed 4.1887902047863905 and Surface Area in meters squared 4.1887902047863905"
