#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  4 10:15:06 2019
HW1_1.py
Kim Banks
@author: kimbanks
"""

#assign pi and e a vaule?

import math as m

pi= m.pi
e= m.e

print('%.4g' % pi)
print('%.4g' % e)
