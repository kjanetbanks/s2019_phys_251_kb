#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 31 10:41:03 2019
HW1_5.py
Kim Banks
@author: kimbanks
"""
#1.5 Problem 5 PART 2
import math as m
def formula_height(T):
    
    G= 6.67*10**-11  #gravitational constant in m^3 kg^-1 s^-1
    M= 5.97*10**24   #mass of Earth in kg
    R= 6.371*10**6   #radius of Earth in meters
    
    
    h= (((G*M*T**2)/(4*m.pi**2))**(1/3))-R
    
    return h

#define any time of T as long as its in units of seconds
    
T= 23.93 *60 *60  #hours*mintues*seconds

formula_height= formula_height(T)

print('Satellite Altitude above Earth is {} meters'.format(formula_height))







