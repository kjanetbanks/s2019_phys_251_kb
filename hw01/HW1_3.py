#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 29 10:52:21 2019
Kim Banks
HW1_3.py
@author: kimbanks
"""
#calc the surface area of sphere radius r=10.0 meters
#output solution in inches^2 and m^2
import math as m

r= 10.0 #radius in meters

sa = (4/3)*m.pi*r**2  #surface area of sphere

#print(sa,(print('Surface Area in m^2 is'))) #try 1

#try 2
print('Surface Area in meters squared is {}'.format(sa))

#part 2 in inches
#convert

r_inches= r*39.37

sa_inches = (4/3)*m.pi*r_inches**2

print('Surface Area in inches squared is {}'.format(sa_inches))