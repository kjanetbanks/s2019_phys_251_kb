#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 11:03:58 2019

@author: kimbanks
"""
n=600851475143 #define n, function and x
def prime(n):
    x=2 #where to start
#removing even numbers since not prime  
    while x*x <=n:
        while n%x==0: #finding prime
            n=n/x
        x=x+1
    if n>1: 
        return(n)
    return(x)    
print(prime(n))    