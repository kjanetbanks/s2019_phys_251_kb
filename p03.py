#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 11:07:39 2019
p03.py
@author: kimbanks
"""
#DISCLAIMER: I DO NOT REMEMBER THE EXACT SYNTAX FOR MOST THINGS
# =============================================================================
#Problem 3 
import matplotlib.pyplot as plt
from scipy.optimize import curv_fit as so
import numpy as np
import math as m

#import file
dname=r'/Users/kimbanks/s2019_phys_251_kb'
fname='harmonic_motion.csv'
dfname='{0:s}/{1:s}'.format(dname,fname)
harm=np.genfromtxt(dfname,delimiter=',',skip_header=1)

print(harm)

#pull out data from file
#time##CANT REMEMBER EXACT SYNTAX
t=[:,0] #seconds
#position
p=[:,1] #meters
#velocity
v=[:,2] #meters per second
A= 0.5
a=-0.01
w= 2*m.pi
B=0
Y=0.1

def func1(A,a,w,B,Y,t):
    y= A*npexp(-a*t)*np.cos(w*t+B)+Y
    return y
print(t,p,v)

so.curv_fit(func1,t,p,v)

#the slope should be the velocity    
plt.plot(t,p,v label='graph')
plt.xlabel('time in secs'. format(t))
plt.ylabel('position in meters'. format(p))
plt.title('time VS positon')
plt.grid()
plt.legend('v')
plt.figure()
plt.show()