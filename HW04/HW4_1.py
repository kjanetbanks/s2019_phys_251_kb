#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 25 11:59:49 2019
HW4_1.py
Kim Banks
@author: kimbanks
"""
import matplotlib.pyplot as plt
import numpy as np    # we import numpy
import scipy.optimize as so

def func1(x,m,b):
     return m*x+b
     
     

dname='/Users/kimbanks/s2019_phys_251_kb' # directory where the file is located
fname=r'data_10.csv' # name of the file - include the extension
dfname ='{0:s}/{1:s}'.format(dname,fname) # concatenate directory and file name
# further information on genfromtxt:
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.genfromtxt.html
data10=np.genfromtxt(dfname,skip_header=1)   # data10 is a ndarray
 # skip one header -> skip_header=1
 # csv             -> delimiter=','
x=np.array(data10[1:,0]) 
y=np.array(data10[1:,1])      

# output the data10, t and y
print(data10)    
print('this is the time {}'.format(x))  
print('This is the position {}'.format(y))
#x=t #defining time as x component
#y=p #defining position as y component
n=x.size
m=np.empty(n)
#defining variables for following equations
sx=0
for i in range(n):
   sx= sx + x[i]
sy=0
for i in range(n):
    sy= sy+ y[i]
sx2=0
for i in range(n):
    sx2= sx2 + x[i]*x[i]  
sxy=0
for i in range(n):
   sxy=sxy + x[i]*y[i]

#ybar=0
#for i in range(n):
ybar= (1/n)*sy
#calc m and b and print them
m=(n*sxy-sx*sy)/(n*sx2-(sx**2))
b=(sx2*sy-sxy*sx)/(n*sx2-(sx**2))
print('This m={}'.format(m))
print('This is b={}'.format(b))
#yhat
yhat=0
for i in range(n):
  yhat=m*x[i]+b
yhaty=0    
#for i in range(n):
yhaty=yhaty+ (yhat-sy)**2 
    
ybary=0    
#for i in range(n):
ybary=ybary+ (ybar-sy)**2

#defining R^2

R2= 1 -(yhaty/ybary)

print('This is R^2={}'.format(R2))


#popt,pcov=so.curve_fit(func1,x,y)
# fit without considering the uncertainty 
popt0,pcov0 =so.curve_fit(func1,x,y)
print('Without uncertainty')
print('Optimization parameter:')
print(popt0)
print('Covariance matrix:')
print(pcov0)
print('\n')

# fit considering the uncertainty 
popt1,pcov1 =so.curve_fit(func1,x,y)
print('With uncertainty')
print('Optimization parameter:')
print(popt1)
print('Covariance matrix:')
print(pcov1)
print('\n')

#plt.fig10.add_subplot(111)
m0=popt0[0]
b0=popt0[1]

m1=popt1[0]
b1=popt1[1]

y2=func1(x,popt0[0],popt0[1])

plt.plot(x,y,y2,'--', label='fit')
plt.ylabel(r'$y-axis$')
plt.xlabel(r'$x-axis$')
plt.grid(which='major',color='black')
plt.title(r'$4.1 Linear Regression$')
plt.legend('xfit',loc='upper left')
plt.plot(x,yhat,label=r'without $\sigma$')
plt.plot(x,ybar,label=r'with $\sigma$')
plt.savefig('HW4_1.png')
plt.show()




