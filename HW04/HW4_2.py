#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 25 12:01:41 2019
HW4_2.py
Kim Banks
@author: kimbanks
"""
import matplotlib.pyplot as plt
import numpy as np    # we import numpy
import scipy.optimize as so

def func2(t,A,a,w):
	V1 = A*np.exp(-a*t)*np.sin(w*t)
    return V1
     

dname='/Users/kimbanks/s2019_phys_251_kb' # directory where the file is located
fname=r'sinusoidal_data.csv' # name of the file - include the extension
dfname ='{0:s}/{1:s}'.format(dname,fname) # concatenate directory and file name
# further information on genfromtxt:
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.genfromtxt.html
sin_data=np.genfromtxt(dfname)   # data10 is a ndarray
 # skip one header -> skip_header=1
 # csv             -> delimiter=','
t=np.array(sin_data[1:,0]) 
V=np.array(sin_data[1:,1])      

# output the data10, t and y


# fit without considering the uncertainty 
popt0,pcov0 =so.curve_fit(func2,t,V)
print('Without uncertainty')
print('Optimization parameter:')
print(popt0)
print('Covariance matrix:')
print(pcov0)
print('\n')

# fit considering the uncertainty 
popt1,pcov1 =so.curve_fit(func2,t,V)
print('With uncertainty')
print('Optimization parameter:')
print(popt1)
print('Covariance matrix:')
print(pcov1)
print('\n')

#plt.fig10.add_subplot(111)


y2=func2(t,popt0[0],popt0[1])

plt.plot(t,V,y2,'--', label='fit')
plt.ylabel(r'$Voltage(V)$')
plt.xlabel(r'$Time(s)$')
plt.grid(which='major',color='black')
plt.title(r'$4.2 Nonlinear Regression$')
plt.legend('tfit',loc='upper left')

plt.savefig('HW4_2.png')
plt.show()
